﻿using System;

namespace ConsoleApplication1
{
    internal class print
    {
        internal static void afiseaza(int v1, int v2, string tip)
        {
            switch (tip)
            {
                case ("par"):
                    if (v1 < v2)
                    {
                        crescator(v1, v2, tip);
                    }
                    else
                    {
                        descrescator(v1, v2, tip);
                    }
                    break;
                case ("impar"):
                    if (v1 < v2)
                    {
                        crescator(v1, v2, tip);
                    }
                    else
                    {
                        descrescator(v1, v2, tip);
                    }
                    break;

                default:
                    break;
            }
        }

        internal static void media(int v1, int v2)
        {

            Console.WriteLine("Media: " + (v1+v2)/2);
        }

        internal static void suma(int v1, int v2)
        {
            Console.WriteLine("Suma: " + (v1+v2));
        }

        internal static void produs(int v1, int v2)
        {
            Console.WriteLine("Produs: " + (v1 * v2));
        }
        
        private static void descrescator(int v1, int v2, string tip)
        {
            for (int i = v1; i > v2; i--)
            {
                if (tip == "par")
                {
                    if (i % 2 == 0)
                    {
                        Console.WriteLine(i);
                    }
                }
                if (tip == "impar")
                {
                    if (i % 2 == 1)
                    {
                        Console.WriteLine(i);
                    }
                }
            }
        }

        private static void crescator(int v1, int v2, string tip)
        {
            for (int i = v1; i < v2; i++)
            {
                if (tip == "par")
                {
                    if (i % 2 == 0)
                    {
                        Console.WriteLine(i);
                    }
                }
                if (tip == "impar")
                {
                    if (i % 2 == 1)
                    {
                        Console.WriteLine(i);
                    }
                }
            }
        }
    }
}